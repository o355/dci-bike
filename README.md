# DCI Electric Bike
Let me explain.

# WHA????
A class of 9th and 10th graders decided...let's do something that involves computers, building, computers, more computers, lots of computers, and requires 4 hex bits to complete (because some people love stripping bits!). The result, an electrical bike.

# The concept
It would consist of a few parts.
* A bike
* Some sort of electric generating system
* That electric generating system would let kids log on, and see what energy they generated.
* Kids would get rewarded for the various amounts of energy they generated.

# How does it work?
* The program uses a straight up loop to let users log in, register, and see other stats. It uses external programs. Yep.
* The information is collected...somehow. We don't know, we'll figure it out later.
* The data is sent back to a MySQL database.

# A few disclaimers
* This isn't the actual in-production code. I don't feel like giving free SQL databases for the internet to use.
* Some code is censored, to not give away private information. Sorry!

# Where's all the code?
* In branches, you silly! The socket branch contains the socket server/client, and the front-end branch is where the GUI goes. Myself, o355 (owen), codes the front end, and Bench (max) codes the back-end (the socket server goooodness).
